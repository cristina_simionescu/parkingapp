/*global require, requirejs */

'use strict';

requirejs.config({
  paths: {
    'angular': ['../lib/angularjs/angular'],
    'angular-route': ['../lib/angularjs/angular-route']
  },
  shim: {
    'angular': {
      exports : 'angular'
    },
    'angular-route': {
      deps: ['angular'],
      exports : 'angular'
    }
  }
});

require(['angular', './controllers', './directives', './filters', './services', 'angular-route'],
  function(angular, controllers) {

    // Declare app level module which depends on filters, and services

    angular.module('parkService', ['parkService.filters', 'parkService.services', 'parkService.directives', 'ngRoute']).
      config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/login', { templateUrl: 'partials/login.html', controller: controllers.login });
        $routeProvider.when('/register', { templateUrl: 'partials/register.html', controller: controllers.register });
        $routeProvider.when('/find', { templateUrl: 'partials/find.html', controller: controllers.find });
        $routeProvider.when('/map', { templateUrl: 'partials/map.html', controller: controllers.map });
        $routeProvider.otherwise({ redirectTo: '/login'});
      }]);

    angular.bootstrap(document, ['parkService']);

});
