/*global define */

'use strict';

define(function() {

/* Controllers */

var controllers = {};

// Login
controllers.login = function($scope, $http) {
  $scope.sendData = function () {
    var email = $scope.user.email;
    var password = $scope.user.password;
    $http.post('/login', { email: email, password: password})
    .then(function(response) {
             window.location.hash = '#/find';
    }, 
    function(response) { // optional
    });
  };

  $scope.register = function() {
    window.location.hash = '#/register';
  };
};
controllers.login.$inject = ['$scope', '$http'];

// Register
controllers.register = function($scope, $http) {
  $scope.reg = function () {
    var fname = $scope.user.fname;
    var lname = $scope.user.lname;
    var phone = $scope.user.phone;
    var email = $scope.user.email;
    var password = $scope.user.password;
    $http.post('/user', { email: email, password: password, fname: fname, lname: lname, phone: phone})
    .then(function(response) {
             window.location.hash = '#/find';
    }, 
    function(response) { // optional
    });
  }
};
controllers.register.$inject = ['$scope', '$http'];

// Find
controllers.find = function($scope, $http, $location) {
    function onGeolocationSuccess(position) {
        var coords = position.coords;
        var query = '?latitude=' + coords.latitude + '&longitude=' + coords.longitude;

        $scope.geolocationInProgress = false;
        // $location.url('/map' + query); // angular is a bitch
        window.location.hash = '#/map' + query; // do it the ninja way
    }
    function onGeolocationError(err) {
        $scope.geolocationInProgress = false;
        console.warn('Geolocation Error (' + err.code + '): ' + err.message);
    }
    var geolocationOptions = {
        // enableHighAccuracy: true,
        // timeout: 5000,
        // maximumAge: 0
    };

    $scope.geolocationInProgress = false;
    $scope.findParking = function() {
        if ($scope.geolocationInProgress) return;
        navigator.geolocation.getCurrentPosition(onGeolocationSuccess, onGeolocationError, geolocationOptions);
        $scope.geolocationInProgress = true;
    };
};
controllers.find.$inject = ['$scope', '$http', '$location'];

// Map
controllers.map = function($scope, $http, $location) {
    function parsePosition(position) {
        return {
            lat: parseFloat(position.latitude),
            lng: parseFloat(position.longitude)
        };
    }
    function makeMarker(map, position, title) {
      var marker = new google.maps.Marker({
        map: map,
        position: position,
        title: title
      });

      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(title);
        infoWindow.open(map, this);
      });
    }
    function makeParkLotTitle(data) {
        return data.name;
    }
    function getOfferSuccess(response) {
        $scope.ad = response.data.ad;
        response.data.offer.forEach(function(parkingLot) {
            makeMarker(map, parsePosition(parkingLot), makeParkLotTitle(parkingLot));
        });
    }
    function getOfferError(response) {
        console.warn('GET offer error: ' + response.status);
    }

    var map = new google.maps.Map(document.getElementById('map'),
        { center: parsePosition($location.search()), zoom: 15 });
    var infoWindow = new google.maps.InfoWindow();

    $http.get('/offer').then(getOfferSuccess, getOfferError);
};
controllers.map.$inject = ['$scope', '$http', '$location'];

return controllers;

});