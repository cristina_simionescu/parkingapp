This project uses the angular-seed-play-java template, which can be found at http://typesafe.com/activator/template/angular-seed-play.
You will need to have activator installed(https://www.typesafe.com/activator/download).
The project can be run by using the following command: activator ~run.
