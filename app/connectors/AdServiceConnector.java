package connectors;

import java.util.Random;

/**
 * Created by Cristina on 21/11/15.
 */
public class AdServiceConnector {

    private static String[] ads = {"Run ma~, run", "In your face!", "Fork a child", "There will be consequences"};
    private static Random rng = new Random();


    public static String getAd() {
        return ads[rng.nextInt(ads.length - 1)];
    }
}
