package connectors;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.F;
import play.libs.ws.WS;

/**
 * Created by Cristina on 21/11/15.
 */
public class ParkingServiceConnector {

    public static F.Promise<JsonNode> getParkingLots() {
        String url = "https://ucn-parking.herokuapp.com/places.json";
        return WS.url(url).get().map(response -> response.asJson());
    }

}
