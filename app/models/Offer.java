package models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import uk.co.panaxiom.playjongo.PlayJongo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

/**
 * Created by Cristina on 20/11/15.
 */
public class Offer {

    @MongoObjectId
    private String _id;
    @MongoObjectId
    private String user_id;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private DateTime time;


    public Offer() {

    }


    public String getId() {
        return _id;
    }

    public String getUserId() {
        return user_id;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    // DAO
    public static MongoCollection offers() {
        return PlayJongo.getCollection("offers");
    }

    public void insert() {
        offers().save(this);
    }

    public void remove() {
        offers().remove(new ObjectId(this.getId()));
    }

    public static Offer findById(String id) {
        return offers().findOne("{_id: #}", new ObjectId(id)).as(Offer.class);
    }

    public static ArrayList<Offer> findByUserId(String user_id) {
        ArrayList<Offer> offerList = new ArrayList<>();
        Iterator<Offer> iterator = offers().find("{user_id: #}", new ObjectId(user_id)).as(Offer.class).iterator();
        while (iterator.hasNext()){
            offerList.add(iterator.next());
        }
        return offerList;
    }
}
