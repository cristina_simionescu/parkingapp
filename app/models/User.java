package models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jongo.MongoCollection;
import org.jongo.marshall.jackson.oid.MongoObjectId;
import uk.co.panaxiom.playjongo.PlayJongo;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Cristina on 20/11/15.
 */

public class User {
    @MongoObjectId
    private String _id;
    private String fname;
    private String lname;
    private String phone;
    private String email;
    private String hash;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private DateTime lastRequest;

    public User() {

    }


    public String getId() {
        return _id;
    }

    public String getFname() {
        return fname;
    }

    public String getPhone() {
        return phone;
    }

    public String getLname() {
        return lname;
    }

    public String getEmail() {
        return email;
    }

    public String getHash() {
        return hash;
    }

    public DateTime getLastRequest() {
        return lastRequest;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setLastRequest(DateTime lastRequest) {
        this.lastRequest = lastRequest;
    }

    // DAO
    public static MongoCollection users() {
        return PlayJongo.getCollection("users");
    }

    public void insert() {
        users().save(this);
    }

    public static void updateLastRequest(String id, DateTime time) {
        users().update("{_id: #}", new ObjectId(id)).with("{$set: {lastRequest: #}}", convertDateTime(time));
    }

    public void remove() {
        users().remove(new ObjectId(this.getId()));
    }

    public static User findById(String id) {
        return users().findOne("{_id: #}", new ObjectId(id)).as(User.class);
    }

    public static User findByEmail(String email) {
        return users().findOne("{email: #}", email).as(User.class);
    }

    public static ArrayList<User> findAll() {
        ArrayList<User> userList = new ArrayList<>();
        Iterator<User> iterator = users().find().as(User.class).iterator();
        while (iterator.hasNext()){
            userList.add(iterator.next());
        }
        return userList;
    }

    private static String convertDateTime(DateTime time) {
        DateTime dateTime = DateTime.parse(time.toString());
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return fmt.print(dateTime);
    }
}