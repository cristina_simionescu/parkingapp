package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.User;
import org.joda.time.DateTime;
import org.mindrot.jbcrypt.BCrypt;
import play.mvc.*;
import play.libs.Json;


/**
 * Created by Cristina on 20/11/15.
 */
public class Users extends Controller {

    public Result getUsers() {
        return ok(Json.toJson(User.findAll()));
    }

    public Result insert() {
        JsonNode requestBody = request().body().asJson();
        String fname = requestBody.get("fname").asText();
        String lname = requestBody.get("lname").asText();
        String email = requestBody.get("email").asText();
        String password = requestBody.get("password").asText();
        String phone = requestBody.get("phone").asText();
        User user = new User();
        user.setFname(fname);
        user.setLname(lname);
        user.setPhone(phone);
        user.setEmail(email);
        user.setHash(BCrypt.hashpw(password, BCrypt.gensalt()));
        user.insert();
        return ok();
    }

    public Result findById(String id) {
        return ok(Json.toJson(User.findById(id)));
    }

    public Result login() {
        JsonNode requestBody = request().body().asJson();
        String email = requestBody.get("email").asText();
        String password = requestBody.get("password").asText();
        try {
            User user = User.findByEmail(email);
            if (BCrypt.checkpw(password, user.getHash())) {
                String token = java.util.UUID.randomUUID().toString();
                response().setCookie("token", token);
                session("userId", user.getId());
                return ok();
            } else {
                return notFound();
            }
        } catch (Exception ex) {
            return notFound();
        }
    }

    public Result logout() {
        session().clear();
        response().discardCookie("token");
        return ok();
    }

}