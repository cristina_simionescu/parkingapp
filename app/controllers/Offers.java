package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import connectors.AdServiceConnector;
import connectors.ParkingServiceConnector;
import models.Offer;
import models.User;
import org.joda.time.DateTime;
import play.libs.F;
import play.mvc.*;
import play.libs.Json;

/**
 * Created by Cristina on 20/11/15.
 */
public class Offers extends Controller {

    public Result findById(String id) {
        return ok(Json.toJson(Offer.findById(id)));
    }

    public Result findByUserId(String userId) {
        return ok(Json.toJson(Offer.findByUserId(userId)));
    }

    public F.Promise<Result> getOffer() {
        String userId = session().get("userId");
        User user = User.findById(userId);
        DateTime lastReq = null;
        try {
            lastReq = user.getLastRequest();
        } catch (Exception ex) {
        }
        if ((lastReq != null && lastReq.isBefore(DateTime.now().minusMinutes(1))) || (lastReq == null)) {
            DateTime time = DateTime.now();
            Offer offer = new Offer();
            offer.setTime(time);
            offer.setUserId(userId);
            offer.insert();
            User.updateLastRequest(userId, time);
            return getOfferWithoutAuthentication();
        }  else {
            return F.Promise.promise(() -> unauthorized());
        }
    }

    public F.Promise<Result> getOfferWithoutAuthentication() {
        return ParkingServiceConnector.getParkingLots().map(lots -> {
            ObjectNode result = Json.newObject();
            result.put("offer", lots);
            result.put("ad", AdServiceConnector.getAd());
            return ok(result);
        });
    }

}