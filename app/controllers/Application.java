package controllers;

import connectors.ParkingServiceConnector;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

    public Result index() {
        ParkingServiceConnector.getParkingLots();
        return ok(index.render());
    }

}
